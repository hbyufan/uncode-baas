package cn.uncode.baas.server.internal.module.data;

import java.util.List;
import java.util.Map;

import cn.uncode.baas.server.cache.SystemCache;
import cn.uncode.baas.server.constant.Resource;
import cn.uncode.baas.server.database.DBContextHolder;
import cn.uncode.baas.server.dto.RestApp;
import cn.uncode.baas.server.exception.ValidateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProxyDataModule implements IDataModule {
	
	@Autowired
	private IDataModule dataSQLModule;
	
	@Autowired
	private IDataModule dataNoSQLModule;

	@Override
	public List<Map<String, Object>> find(Object param)
			throws ValidateException {
		if(isMongoDB()){
			return dataNoSQLModule.find(param);
		}
		return dataSQLModule.find(param);
	}

	@Override
	public Map<String, Object> findOne(Object param) throws ValidateException {
		if(isMongoDB()){
			return dataNoSQLModule.findOne(param);
		}
		return dataSQLModule.findOne(param);
	}

	@Override
	public int update(Object param) throws ValidateException {
		if(isMongoDB()){
			return dataNoSQLModule.update(param);
		}
		return dataSQLModule.update(param);
	}

	@Override
	public int remove(Object param) throws ValidateException {
		if(isMongoDB()){
			return dataNoSQLModule.remove(param);
		}
		return dataSQLModule.remove(param);
	}

	@Override
	public Object insert(Object param) throws ValidateException {
		if(isMongoDB()){
			return dataNoSQLModule.insert(param);
		}
		return dataSQLModule.insert(param);
	}

	@Override
	public int count(Object param) throws ValidateException {
		if(isMongoDB()){
			return dataNoSQLModule.count(param);
		}
		return dataSQLModule.count(param);
	}
	
	private boolean isMongoDB() {
		boolean result = true;
		RestApp app = SystemCache.getRestApp(DBContextHolder.getDbType());
		if(null != app){
			if(Resource.REST_APP_DB_TYPE_MYSQL == app.getDbType()){
				result = false;
			}
		}
		return result;
	}

}
